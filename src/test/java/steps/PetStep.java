package steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Assert;


import com.rashmil.petstore.petAPI.Category;
import com.rashmil.petstore.petAPI.Order;
import com.rashmil.petstore.petAPI.Pet;
import com.rashmil.petstore.petAPI.Tag;
import static org.hamcrest.Matchers.*;

public class PetStep {

	
	@Given("^user is able to ping the base url$")
	public void user_is_able_to_ping_the_base_url() {
		given().relaxedHTTPSValidation().when().get().then().statusCode(200);
		
	}
	
	@When("^user creates POST request$")
	public void user_creates_POST_request(DataTable dataTable)  {
		
	  Pet pet=new Pet();
	 for(Map<String,String> map: dataTable.asMaps(String.class, String.class)) {
	 pet.setId(Integer.parseInt(map.get("id")));
	 pet.setName(map.get("name"));
	 Category cat=new Category();
	 cat.setId(Integer.parseInt(map.get("category_id")));
	 cat.setName(map.get("category_name"));
	 pet.setCategory(cat);
	 List<String> photoUrls=new ArrayList<String>();
	
	for(String photoURL:map.get("photoUrls").split(" ")) {
		photoUrls.add(photoURL);
	}
	pet.setPhotoUrls(photoUrls);
	Tag tag=new Tag();
	tag.setId(Integer.parseInt(map.get("tag_id")));
	tag.setName(map.get("tag_name"));
	List<Tag> tags=new ArrayList<Tag>();
	tags.add(tag);
	pet.setTags(tags);
	pet.setStatus(map.get("status"));
	 
	given().contentType(ContentType.JSON).relaxedHTTPSValidation().when().body(pet).log().all().post("/v2/pet/").then().statusCode(200).body("status", equalTo(map.get("status"))).body("id", equalTo(Integer.parseInt(map.get("id"))));
	
	
	
	given().relaxedHTTPSValidation().pathParam("id", map.get("id")).log().all().when().get("/v2/pet/{id}").then().statusCode(200);
	
	 }
	 
	 }
	


	@Then("^verify user is able to delete pet with \"([^\"]*)\"$")
	public void verify_user_is_able_to_delete_pet_with(String id)  {
		
		given().relaxedHTTPSValidation().pathParam("id", id).log().all().when().delete("/v2/pet/{id}").then().statusCode(200);

		given().relaxedHTTPSValidation().pathParam("id", id).log().all().when().get("/v2/pet/{id}").then().statusCode(404);
		//Assert.assertTrue(false);
		
	}
	
	@When("^user places an order$")
	public void user_places_an_order(DataTable dataTable)  {
		 Order order=new Order();
		 for(Map<String,String> map: dataTable.asMaps(String.class, String.class)) {
		order.setId(Integer.parseInt(map.get("id")));
		order.setPetId(Integer.parseInt(map.get("petId")));
		 order.setQuantity(Integer.parseInt(map.get("quantity")));
		 order.setShipDate(map.get("shipDate"));
		 order.setStatus(map.get("status"));
		 order.setComplete(Boolean.parseBoolean(map.get("complete")));
		 
		 
		given().contentType(ContentType.JSON).relaxedHTTPSValidation().when().body(order).log().all().post("/v2/store/order").then().statusCode(200).body("status", equalTo(map.get("status")));
		
		
		
		
		
		given().relaxedHTTPSValidation().pathParam("id", map.get("id")).log().all().when().get("/v2/store/order/{id}").then().statusCode(200);
		
		 }
		 


}

}