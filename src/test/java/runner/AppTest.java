package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Unit test for simple App.
 */
//If want to run the third test as well then tags value can be tags={"@tag1,@tag2"}
//if want to run via command prompt give base url mvn test -DbaseUri="https://petstore.swagger.io" test
//Currently https://petstore.swagger.io/v2/store/order/2 giving 500 so unable to test
//Running from command prompt mvn test -Dcucumber.options="--tags @Tag1" -DbaseUri="https://petstore.swagger.io"

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features",glue="steps",plugin= {"pretty", "junit:target/cucumber.xml", "json:target/cucumber.json","html:target"},tags= {"@Tag1,@Tag2"})
public class AppTest {

   
    
}
