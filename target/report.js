$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Test.feature");
formatter.feature({
  "line": 2,
  "name": "Test API",
  "description": "",
  "id": "test-api",
  "keyword": "Feature"
});
formatter.before({
  "duration": 262601000,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Add new pet",
  "description": "",
  "id": "test-api;add-new-pet",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@Tag1"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "user is able to ping the base url",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "user creates POST request",
  "rows": [
    {
      "cells": [
        "id",
        "category_id",
        "category_name",
        "photoUrls",
        "tag_id",
        "tag_name",
        "status",
        "name"
      ],
      "line": 8
    },
    {
      "cells": [
        "3",
        "3",
        "category_3",
        "photo",
        "3",
        "tag_3",
        "available",
        "ras"
      ],
      "line": 9
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "PetStep.user_is_able_to_ping_the_base_url()"
});
formatter.result({
  "duration": 3500175400,
  "status": "passed"
});
formatter.match({
  "location": "PetStep.user_creates_POST_request(DataTable)"
});
formatter.result({
  "duration": 5146046500,
  "status": "passed"
});
formatter.before({
  "duration": 286300,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Delete existing pet",
  "description": "",
  "id": "test-api;delete-existing-pet",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@Tag1"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "user is able to ping the base url",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "verify user is able to delete pet with \"3\"",
  "keyword": "Then "
});
formatter.match({
  "location": "PetStep.user_is_able_to_ping_the_base_url()"
});
formatter.result({
  "duration": 1647319100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3",
      "offset": 40
    }
  ],
  "location": "PetStep.verify_user_is_able_to_delete_pet_with(String)"
});
formatter.result({
  "duration": 4694410400,
  "status": "passed"
});
formatter.before({
  "duration": 234100,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Place and order",
  "description": "",
  "id": "test-api;place-and-order",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 16,
      "name": "@Tag2"
    }
  ]
});
formatter.step({
  "line": 18,
  "name": "user is able to ping the base url",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "user places an order",
  "rows": [
    {
      "cells": [
        "id",
        "petId",
        "quantity",
        "shipDate",
        "status",
        "complete"
      ],
      "line": 20
    },
    {
      "cells": [
        "6",
        "3",
        "3",
        "2020-03-04",
        "placed",
        "true"
      ],
      "line": 21
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "PetStep.user_is_able_to_ping_the_base_url()"
});
formatter.result({
  "duration": 2637703800,
  "status": "passed"
});
formatter.match({
  "location": "PetStep.user_places_an_order(DataTable)"
});
formatter.result({
  "duration": 2660425000,
  "error_message": "java.lang.AssertionError: 1 expectation failed.\nExpected status code \u003c200\u003e but was \u003c500\u003e.\n\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.codehaus.groovy.reflection.CachedConstructor.invoke(CachedConstructor.java:83)\r\n\tat org.codehaus.groovy.reflection.CachedConstructor.doConstructorInvoke(CachedConstructor.java:77)\r\n\tat org.codehaus.groovy.runtime.callsite.ConstructorSite$ConstructorSiteNoUnwrap.callConstructor(ConstructorSite.java:84)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCallConstructor(CallSiteArray.java:60)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callConstructor(AbstractCallSite.java:235)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callConstructor(AbstractCallSite.java:247)\r\n\tat io.restassured.internal.ResponseSpecificationImpl$HamcrestAssertionClosure.validate(ResponseSpecificationImpl.groovy:471)\r\n\tat io.restassured.internal.ResponseSpecificationImpl$HamcrestAssertionClosure$validate$1.call(Unknown Source)\r\n\tat io.restassured.internal.ResponseSpecificationImpl.validateResponseIfRequired(ResponseSpecificationImpl.groovy:643)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoMetaMethodSite$PogoCachedMethodSiteNoUnwrapNoCoerce.invoke(PogoMetaMethodSite.java:210)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoMetaMethodSite.callCurrent(PogoMetaMethodSite.java:59)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:166)\r\n\tat io.restassured.internal.ResponseSpecificationImpl.statusCode(ResponseSpecificationImpl.groovy:122)\r\n\tat io.restassured.specification.ResponseSpecification$statusCode$0.callCurrent(Unknown Source)\r\n\tat io.restassured.internal.ResponseSpecificationImpl.statusCode(ResponseSpecificationImpl.groovy:130)\r\n\tat io.restassured.internal.ValidatableResponseOptionsImpl.statusCode(ValidatableResponseOptionsImpl.java:117)\r\n\tat steps.PetStep.user_places_an_order(PetStep.java:93)\r\n\tat ✽.When user places an order(Test.feature:19)\r\n",
  "status": "failed"
});
});