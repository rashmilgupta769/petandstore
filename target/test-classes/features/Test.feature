
Feature: Test API

@Tag1
Scenario: Add new pet
Given user is able to ping the base url
When user creates POST request
|id|category_id|category_name|photoUrls|tag_id|tag_name|status|name|
|3|3|category_3|photo|3|tag_3|available|ras|

@Tag1
Scenario: Delete existing pet
Given user is able to ping the base url
Then verify user is able to delete pet with "3"

@Tag2
Scenario: Place and order
Given user is able to ping the base url
When user places an order
|id|petId|quantity|shipDate|status|complete|
|6|3|3|2020-03-04|placed|true|
